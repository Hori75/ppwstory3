from django.urls import include, path

from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('profile/', views.profile, name="profile"),
    path('posts/', views.posts, name="posts"),
    path('posts/<int:number>', views.posts, name="posts")
]
